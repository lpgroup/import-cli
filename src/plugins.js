const {
  addPluginWithOptions,
  closePlugins,
  onPluginsReady,
  onPluginReady,
  axios,
  nats,
  rabbitmq
} = require("@lpgroup/feathers-utils");

function setupPlugins(config) {
  addPluginWithOptions("axios", axios, {
    baseURL: config.http.server,
    customHeader: "import-cli",
    errorHandlerWithException: false,
    requiredKeys: config.requiredKeys,
    ignoreKeyCompare: config.ignoreKeyCompare,
    user: config.http.user,
    password: config.http.password,
    headers: config.http.headers
  });

  addPluginWithOptions("nats", nats, {
    name: "import-cli",
    uri: config.nats.uri,
    queue: config.nats.queue,
    messagePrefix: config.nats.messagePrefix,
    ignoreKeyCompare: config.ignoreKeyCompare
  });

  addPluginWithOptions("rabbitmq", rabbitmq, {
    uri: config.rabbitmq.uri,
    queue: config.rabbitmq.queue
  });

  return onPluginsReady();
}

module.exports = {
  setupPlugins,
  closePlugins,
  axios: async () => await onPluginReady("axios"),
  nats: async () => await onPluginReady("nats"),
  rabbitmq: async () => await onPluginReady("rabbitmq")
};
