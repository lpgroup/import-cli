const { program } = require("commander");
const path = require("path");
const fs = require("fs");
const Table = require("table-layout");

function getCmdLineArgs() {
  program
    .version("0.0.1", "--version", "output the current version")

    .requiredOption("-e, --environment <env>", "Environment from .import.json to use", "default")
    .requiredOption("-w, --cwd <folder>", "Folder to import from", "./import")
    .requiredOption("-i, --include <reqexp>", "Regexp to files to only include", ".js$")
    .requiredOption("-x, --exclude <reqexp>", "Regexp to files to exclude", "")
    .option("-v, --verbose", "Regexp to files to exclude", false)
    .parse(process.argv);

  program.cwd = path.resolve(program.cwd);
  if (!fs.existsSync(program.cwd)) {
    console.error(`Folder doesn't exist: ${program.cwd}`);
  }
  return program;
}

function getConfig(env) {
  let config = {};
  const filePath = path.resolve(".import.json");
  if (fs.existsSync(filePath)) {
    config = require(filePath);
  }

  if (config.environments && config.environments[env])
    return {
      ...config.environments[env],
      requiredKeys: config.requiredKeys,
      ignoreKeyCompare: config.ignoreKeyCompare
    };
  else throw Error("Environment doesn't exist in .import.json");
}

function printParameters(parameters, config) {
  console.log(`import-cli (${parameters.version()})`);
  const table = new Table(
    [
      { name: "Environment:", value: parameters.env },
      { name: "CWD:", value: parameters.cwd },
      { name: "Include:", value: parameters.include },
      { name: "Exclude:", value: parameters.exclude || "NA" },
      { name: "Verbose:", value: parameters.verbose },
      { name: "http.server:", value: config.http.server },
      { name: "http.user:", value: config.http.user },
      { name: "http.password:", value: "***" },
      { name: "http.headers:", value: JSON.stringify(config.http.headers) },

      { name: "nats.uri:", value: config.nats.uri },
      { name: "nats.queue:", value: config.nats.queue },
      { name: "nats.messagePrefix:", value: config.nats.messagePrefix },

      { name: "rabbitmq.uri:", value: config.rabbitmq.uri },
      { name: "rabbitmq.queue:", value: config.rabbitmq.queue }
    ],
    { maxWidth: 100 }
  );
  console.log(table.toString());
}

module.exports = { getCmdLineArgs, getConfig, printParameters };
