const { sortBy } = require("lodash");
const requireDirectory = require("require-directory");

/**
 * List of imported/required javascript files.
 *    [{obj, relativePath, Filename}]
 *    obj - module.export of imported file
 */
const _filesToImport = {};

function getModules(cwd, include, exclude) {
  requireDirectory(module, cwd, {
    include: _include(include, exclude),
    visit: _visit(cwd)
  });

  return _filesToImport;
}

/**
 * Filter (include, exclude) files that should be imported.
 *
 * Return True if file should be imported/included.
 */
function _include(include, exclude) {
  return (path) => {
    const reInclude = RegExp(include);
    const reExclude = RegExp(exclude);
    return (
      !/config.js/.test(path) &&
      !/\/src\//.test(path) &&
      reInclude.test(path) &&
      (!exclude || !reExclude.test(path))
    );
  };
}

/**
 * Create a list of files that should be imported.
 *
 * This is executed for each file that is imported.
 */
function _visit(cwd) {
  return (obj, joined, fileName) => {
    const relativePath = joined.substring(cwd.length);
    const folder = relativePath.substring(0, relativePath.length - fileName.length);
    if (folder in _filesToImport) _filesToImport[folder].push({ obj, relativePath, fileName });
    else _filesToImport[folder] = [{ obj, relativePath, fileName }];
  };
}

/**
 * Execute main function in all imported files.
 * Functions/files in the same folder will be executed synchronous.
 * Different folder will be executed asyncronous.
 */
async function runAllImports(filesToImport) {
  let asyncImports = [];
  console.log("Scanning folder for imports");
  for (const key in filesToImport) {
    console.log(`  ${key}`);
    asyncImports.push(async () => {
      // Import files in one folder synchronous.
      const sortedFiles = sortBy(filesToImport[key], "fileName");
      for (const fileObj of sortedFiles) {
        console.log(`File: ${fileObj.relativePath}`);
        try {
          await fileObj.obj();
        } catch (err) {
          console.error(err.message);
        }
      }
    });
  }

  return await Promise.all(asyncImports.map((func) => func()));
}

module.exports = { getModules, runAllImports };
