#!/usr/bin/env node

// Needed by local packages installed with "npm link" to be able
// to find it's dependencies.
if (process.env.NODE_ENV != "PRODUCTION") {
  process.env.NODE_PATH = require("path").resolve("./node_modules");
  require("module").Module._initPaths();
}

const debugSettings = require("debug");
const { getCmdLineArgs, getConfig, printParameters } = require("./src/parameters");
const { getModules, runAllImports } = require("./src/import");
const { setupPlugins, closePlugins } = require("./src/plugins");

function main() {
  const parameters = getCmdLineArgs();
  const config = getConfig(parameters.environment);
  console.time("Time");
  printParameters(parameters, config);

  if (parameters.verbose) {
    // Axios output to console.
    debugSettings.enable(debugSettings.load() || "" + ",axios,axios:error,nats,rabbitmq,compare");
    debugSettings.save();
  }

  // The script can handle one server of each type
  setupPlugins(config).then(() => {
    const filesToImport = getModules(parameters.cwd, parameters.include, parameters.exclude);
    runAllImports(filesToImport).then(() => {
      console.log("Done");
      console.timeEnd("Time");
      closePlugins();
    });
  });
}

try {
  main();
} catch (error) {
  console.error(error.message);
  console.error(error);
}
