# @lpgroup/import-cli

Cli command that imports data into rest api, RabbitMQ server and NATS.

## Install

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/import-cli

```

## Configuration

.import.json

```javascript
{
  "int": {
    "http": { "server": "https://api.thecatapi.com/v1/", "user": "admin", "password": "xx" },
    "nats": { "server": "nats://localhost:4222", "queue": "import-js", "prefix": "int" },
    "rabbitmq": { "uri": "amqps://user:password@wolf.rmq.cloudamqp.com/server-xxx" }
  }
}
```

## Example

Run script from command line.

```bash
import-cli -s http://localhost:8080
npm run import -- -v-s http://localhost:8080
```

## Contribute

See https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md

## License

MIT, see LICENSE.MD
